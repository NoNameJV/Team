# Team

> Just clone git and double click on index.html 
```
git clone https://gitlab.com/NoNameJV/Team.git
``` 

# Roadmap 

- Finalize team design.
- Correct the "We are hiring" section.
- Add social accounts.
- Add final email.
- Add animation on team member displaying

# Nginx exemple conf 

```
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    default_type  application/octet-stream;
    include  mime.types;
    sendfile  on;
    keepalive_timeout  30;

    server {
        listen 3000;
        root E:/NoNameJV/Team;
        server_name localhost;

        location / {
            index index.html;
        }

        location /static/ {
            try_files $uri /static;
        }

        location ~ \.css {
            add_header Content-Type text/css;
        }

        location ~ \.js {
            add_header Content-Type application/x-javascript;
        }

    }

}
```