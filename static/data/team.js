team_data = [
    {
        "avatar": "static/images/avatars/adrien.jpg",
        "name": "VENNET Adrien",
        "nickname": "Demogia",
        "job": "Project leader, Game-designer"
    },
    {
        "avatar": "static/images/avatars/math.jpg",
        "name": "DAUBRY Mathieu",
        "nickname": "Math",
        "job": "Marketing & Communication"
    },
    {
        "avatar": "static/images/avatars/fraxken.png",
        "name": "GENTILHOMME Thomas",
        "nickname": "Fraxken",
        "job": "Full-stack developer"
    },
    {
        "avatar": "static/images/avatars/nopoza.png",
        "name": "TINCO Rémy",
        "nickname": "Nopoza",
        "job": "System administrator"
    },
    {
        "avatar": "static/images/avatars/noavatar.gif",
        "name": "STOUDER Xavier",
        "nickname": "Xstoudi",
        "job": "Full-stack Developer"
    },
    {
        "avatar": "static/images/avatars/noavatar.gif",
        "name": "POISSEAU Théotime",
        "nickname": "Purexo",
        "job": "Full-stack Developer"
    },
    {
        "avatar": "static/images/avatars/nattack.jpg",
        "name": "DEBOUCHAGE Antoine",
        "nickname": "Nattack",
        "job": "Game programmer"
    },
    {
        "avatar": "static/images/avatars/captainfive.jpg",
        "name": "MONTES Irvin",
        "nickname": "Captainfive",
        "job": "Compositor, Sound-designer"
    },
    {
        "avatar": "static/images/avatars/noavatar.gif",
        "name": "ODERMATT Ramon",
        "nickname": "Araylos",
        "job": "Artist"
    }
]