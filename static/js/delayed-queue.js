function DelayedQueue(timeout) {
    this.timeout = timeout || 1000;
    this.queue = []
    this._id = null;
}

DelayedQueue.prototype.add = function DelayedQueue_prototype_add(callback) {
    this.queue.push(callback);
}

DelayedQueue.prototype._runFirst = function DelayedQueue_prototype__runFirst() {
    var callback = this.queue.shift();

    return callback ? callback() : clearInterval(this._id);
}

DelayedQueue.prototype.run = function DelayedQueue_prototype_run() {
    this._runFirst();
    this._id = setInterval(this._runFirst.bind(this), this.timeout);
}