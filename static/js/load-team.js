// node team
var team = document.querySelector('.team');

function flushNode(node) {
    while (node.hasChildNodes()) {
        node.removeChild(node.firstChild);
    }
}

function attachChildren(node, target) {
    while (node.hasChildNodes()) {
        target.appendChild(node.firstChild)
    }
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}

flushNode(team);
shuffleArray(team_data);

// tpl team to hydrate
var tplTeam = document.getElementById('tpl-team').cloneNode(true);

// create queu-list-delayed (500ms between actions)
var queue = new DelayedQueue(500);

// itrate on team member
for (var i = 0; i < team_data.length; i += 2) {
    // populate 2 per 2
    var users = [team_data[i], team_data[i+1]];

    // add process to queu
    queue.add(function() {
        // iterate on 2 per 2 populate member
        this.forEach(function(user) {
            if (!user) return;

            // clone tpl
            var tpl = tplTeam.cloneNode(true);

            // hydrate avatar
            var avatar = tpl.querySelector('img');
            avatar.setAttribute('src', user.avatar);
            avatar.setAttribute('alt', user.nickname);

            // hydrate name
            var name = tpl.querySelector('h3');
            name.textContent = user.name;

            // hydrate nickname
            var nickname = tpl.querySelector('strong');
            nickname.textContent = user.nickname;

            // hydrate job
            var job = tpl.querySelector('.bar p');
            job.textContent = user.job;

            // append tpl to team node
            attachChildren(tpl, team);
        })
    }.bind(users)/* Little trick for isolate users from scope - bad var ... not use let or const for ES5 compatibility */);
}

// run process
queue.run();